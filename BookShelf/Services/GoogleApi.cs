﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Books.v1;
using Google.Apis.Services;
using Microsoft.Extensions.Configuration;

namespace BookShelf.Services
{
    public class GoogleApi
    {
        private static IConfiguration _configuration;

        private static string  _projectName;
        private static string _apiKey;

        public GoogleApi()
        {
            _projectName = _configuration.GetValue<string>("Services:GoogleBooks:ProjectName");
            _apiKey = _configuration.GetValue<string>("Services:GoogleBooks:ApiKey");
        }

        public static BooksService BooksService = new BooksService(
            new BaseClientService.Initializer
            {
                ApplicationName = _projectName,
                ApiKey = _apiKey
            });
    }
}
