﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function AddFavorite(googleId) {
    $("#" + googleId + " .card-body").loading('start');
    $.ajax({
        type: 'GET',
        url: (location.protocol + "//" + location.host + '/Books/AddFavorite' ),
        data: {
            "googleId": googleId
        },
        success: function (data) {
            $("#" + googleId + " .card-body").loading('stop');
            if (data == "Exist") {
                $.snackbar({ content: "Erro, esse livro já está na sua lista." });
            }

            if (data == true) {
                $.snackbar({ content: "Livro adicionado aos favoritos!" });
            }

            if (data == false) {
                $.snackbar({ content: "Erro, tente novamente!" });
            }
        },
        error: function () {
            $.snackbar({ content: "Erro, tente novamente!" });
        }
    });
}

function RemoveFavorite(googleId) {

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-raised btn-success m-1',
            cancelButton: 'btn btn-raised btn-danger m-1'
        },
        buttonsStyling: false
    });

    swalWithBootstrapButtons.fire({
        title: 'Você tem certeza?',
        text: "Você não poderá reverter isso!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sim, remova!',
        cancelButtonText: 'Não, cancelar!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $("#books-list").loading('start');
            $.ajax({
                type: 'GET',
                url: (location.protocol + "//" + location.host + '/Books/RemoveFavorite'),
                data: {
                    "googleId": googleId
                },
                success: function (data) {
                    if (data) {
                        $.ajax({
                            type: 'GET',
                            url: (location.protocol + "//" + location.host + '/Books/FavoriteList'),
                            dataType: 'html',
                            success: function (favoritepage) {

                                $("#books-list").empty();
                                $("#books-list").append($(favoritepage).find("#books-list").contents());
                                $("#books-list").loading('stop');
                                $.snackbar({ content: "Livro removido dos favoritos." });

                            }
                        });

                    } else {
                        $.snackbar({ content: "Erro, tente novamente!" });
                    }
                },
                error: function() {
                    $.snackbar({ content: "Erro, tente novamente!" });
                }
            });
        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            $.snackbar({ content: "Cancelado." });
        }
    });

    
}