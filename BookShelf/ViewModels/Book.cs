﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShelf.ViewModels
{
    public class Book
    {
        public string Id { get; set; }
        public string Thumbnail { get; set; }
        public string Titile { get; set; }
        public string Description { get; set; }
        public string PublishedDate { get; set; }
        
    }
}
