﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShelf.ViewModels
{
    public class BookDetail : Book
    {
        public List<string> Autors { get; set; }
        public string Publisher { get; set; }
        public List<string> Isbn { get; set; }
        public List<Book> RecommendationList { get; set; }

        public BookDetail()
        {
            Autors = new List<string>();
            Isbn = new List<string>();
            RecommendationList = new List<Book>();
        }
    }
}
