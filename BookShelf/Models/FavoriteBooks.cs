﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace BookShelf.Models
{
    public class FavoriteBooks 
    {
        [Key]
        public int FavoriteId { get; set; }

        public string GoogleBookId { get; set; }
        public string Thumbnail { get; set; }
        public string Titile { get; set; }
        public string Description { get; set; }
        public string PublishedDate { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
