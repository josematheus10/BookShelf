﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;

namespace BookShelf.Models
{
    public class ApplicationUser : IdentityUser
    { 
        public ApplicationUser() : base()
        {

        }
       

        public virtual ICollection<FavoriteBooks> FavoriteBooks { get; set; }
    }
}
