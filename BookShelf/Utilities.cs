﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShelf
{
    public class Utilities
    {
        public static string Truncate(string text, int maxLength, string suffix = "...")
        {
            string str = text;
            int length = maxLength - suffix.Length;

            if (maxLength <= 0) return str;
            
            if (length <= 0)return str;

            if ((text != null) && (text.Length > maxLength))
            {
                return (text.Substring(0, length).TrimEnd(new char[0]) + suffix);
            }
            return str;
        }
    }
}
