﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BookShelf.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BookShelf.Models;
using BookShelf.Services;
using BookShelf.ViewModels;
using Google.Apis.Books.v1;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace BookShelf.Controllers
{
    public class BooksController : Controller
    {
        private readonly ILogger<BooksController> _logger;
        private readonly Random _random = new Random();
        private UserManager<IdentityUser> _user;
        private ApplicationDbContext _context;


        public BooksController(ILogger<BooksController> logger, UserManager<IdentityUser> userManager, ApplicationDbContext context)
        {

            _logger = logger;
            _user = userManager;
            _context = context;
        }

        public async Task<IActionResult> Index(string search, int currentpage = 1)
        {
            if (string.IsNullOrEmpty(search))
            {
                int num = _random.Next(0, 26);
                char let = (char)('a' + num);
                search = let.ToString();
            }

            var apiRequest = GoogleApi.BooksService.Volumes.List(search);

            apiRequest.UserIp = HttpContext.Connection.RemoteIpAddress.ToString();
            apiRequest.OrderBy = VolumesResource.ListRequest.OrderByEnum.Relevance;
            apiRequest.LibraryRestrict = VolumesResource.ListRequest.LibraryRestrictEnum.MyLibrary;
            apiRequest.Projection = VolumesResource.ListRequest.ProjectionEnum.Lite;
            apiRequest.LangRestrict = "pt-BR";
            apiRequest.ShowPreorders = false;
            apiRequest.MaxResults = 40;
            apiRequest.StartIndex = (currentpage == 1 ? 0 : (40 * currentpage));

            var result = await apiRequest.ExecuteAsync();

            ViewBag.currentpage = currentpage;
            ViewBag.search = search;
            ViewBag.pagesize = 0;
            ViewBag.totalResult = 0;

            List<Book> books = new List<Book>();

            if (result.Items != null)
            {
                foreach (var book in result.Items)
                {
                    Book bookObj = new Book()
                    {
                        Id = book.Id,
                        PublishedDate = book.VolumeInfo.PublishedDate,
                        Thumbnail = book.VolumeInfo.ImageLinks?.Medium ?? book.VolumeInfo.ImageLinks?.Thumbnail,
                        Titile = book.VolumeInfo.Title,
                        Description = book.VolumeInfo.Description
                    };
                    books.Add(bookObj);
                    ViewBag.totalResult = ViewBag.totalResult + 1;
                }

                ViewBag.pagesize = result.TotalItems;
            }

            return View(books);
        }

        public async Task<IActionResult> Detail(string Id)
        {
            var apiRequest = GoogleApi.BooksService.Volumes.Get(Id);
            var book = await apiRequest.ExecuteAsync();
            BookDetail bookObj = new BookDetail()
            {
                Id = book.Id,
                PublishedDate = book.VolumeInfo.PublishedDate,
                Thumbnail = book.VolumeInfo.ImageLinks?.Medium ?? book.VolumeInfo.ImageLinks?.Thumbnail,
                Titile = book.VolumeInfo.Title,
                Description = book.VolumeInfo.Description,
                Publisher = book.VolumeInfo.Publisher
            };

            if (book.VolumeInfo.IndustryIdentifiers != null)
            {
                foreach (var isbn in book.VolumeInfo.IndustryIdentifiers)
                {
                    if (isbn.Identifier != null)
                        bookObj.Isbn.Add(isbn.Identifier);
                }
            }

            if (book.VolumeInfo.Authors != null)
            {
                foreach (var autor in book.VolumeInfo.Authors)
                {
                    if (autor != null)
                        bookObj.Autors.Add(autor);
                }
            }

            return View(bookObj);
        }

        public async Task<IActionResult> FavoriteList()
        {
            List<Book> books = new List<Book>();

            if (User.Identity.IsAuthenticated)
            {
                IdentityUser user = await _user.GetUserAsync(User);

                var favoriteList = _context.FavoriteBooks.Where(x => x.UserId == user.Id);

                if (favoriteList.Any())
                {
                    foreach (var favorite in favoriteList)
                    {
                        Book bookObj = new Book()
                        {
                            Id = favorite.GoogleBookId,
                            PublishedDate = favorite.PublishedDate,
                            Thumbnail = favorite.Thumbnail,
                            Titile = favorite.Titile,
                            Description = favorite.Description
                        };
                        books.Add(bookObj);
                    }
                }
            }

            return View(books);
        }

        public async Task<JsonResult> AddFavorite(string googleId)
        {
            try
            {
                 if (string.IsNullOrEmpty(googleId))
                   throw new Exception("Not id");

                 if (User.Identity.IsAuthenticated)
                 {
                     IdentityUser user = await _user.GetUserAsync(User);


                     var data = _context.FavoriteBooks.Where(x => x.UserId == user.Id && x.GoogleBookId == googleId);

                     if (!data.Any())
                     {
                         var apiRequest = GoogleApi.BooksService.Volumes.Get(googleId);
                         var book = await apiRequest.ExecuteAsync();
                         if (book != null)
                         {
                             FavoriteBooks bookObj = new FavoriteBooks()
                             {
                                 UserId = user.Id,
                                 GoogleBookId = book.Id,
                                 PublishedDate = book.VolumeInfo.PublishedDate,
                                 Thumbnail = book.VolumeInfo.ImageLinks?.Medium ?? book.VolumeInfo.ImageLinks?.Thumbnail,
                                 Titile = book.VolumeInfo.Title,
                                 Description = book.VolumeInfo.Description
                             };

                             _context.FavoriteBooks.Add(bookObj);
                             _context.SaveChanges();
                         }
                     }
                     else
                     {
                         return Json("Exist");
                     }
                    
                     return Json(true);
                 }
                 else
                 {
                     throw new Exception("User not login");
                 }
            }
            catch (Exception )
            {
                return Json(false);
            }
        }

        public async Task<JsonResult> RemoveFavorite(string googleId)
        {
            try
            {
                if (string.IsNullOrEmpty(googleId))
                    throw new Exception("Not id");

                if (User.Identity.IsAuthenticated)
                {
                    IdentityUser user = await _user.GetUserAsync(User);

                    var data = _context.FavoriteBooks.Where(x => x.UserId == user.Id && x.GoogleBookId == googleId);

                    if (!data.Any()) return Json(true);

                    _context.FavoriteBooks.RemoveRange(data);
                    _context.SaveChanges();

                    return Json(true);
                }
                else
                {
                    throw new Exception("User not login");
                }
            }
            catch (Exception )
            {
                return Json(false);
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
